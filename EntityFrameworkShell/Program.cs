﻿using Newtonsoft.Json;
using System;
using System.Data.Entity;
using System.Linq;

namespace EntityFrameworkShell
{
	class Program
	{
		private class Person
		{
			public int Id { get; set; }

			public string Name { get; set; }

			public int Age { get; set; }
		}

		private class Context : DbContext
		{
			public Context() : base("DefaultConnection") { }

			public DbSet<Person> Person { get; set; }
		}

		static void Main(string[] args)
		{
			var context = new Context();

			Console.WriteLine(JsonConvert.SerializeObject(context.Person.ToList(), Formatting.Indented));

			context.Person.Add
			(
				new Person
				{
					Name = "Foo",
					Age = 100
				}
			);
			context.SaveChanges();

			Console.WriteLine(JsonConvert.SerializeObject(context.Person.ToList(), Formatting.Indented));
		}
	}
}